from bottle import route, run, template, static_file, request
import bottle
from model.model import Model
from parsing.utils import ParseException
import utils

import parsing.parser as parsers_A
import parsing.lectures_meta as parsers_B

bottle.BaseRequest.MEMFILE_MAX = 10 * 1024 ** 2  # max 10MB post payload


@route('/static/:path#.+#', name='static')
def static(path):
    return static_file(path, root='static')


@route("/posthere", method="POST")
def post_resource():
    pastedtext = request.json['pastedtext']
    with open("logging.txt", 'w') as f:
        f.write(str(pastedtext))
    cells = utils.clipboard_to_cells(pastedtext)
    with open("celled.txt", 'w') as f:
        f.write(str(cells))

    model = Model()
    output = []

    for cells_of_one_tab in cells:
        cell_parsers = [parsers_A.ParseSemester, parsers_B.Parse3rd, parsers_B.Parse4th]
        for cell_parser in cell_parsers:
            try:
                tmp_result = cell_parser(cells_of_one_tab, model)
                if not tmp_result:
                    continue
                output.append(tmp_result)
                break
            except ParseException:
                pass
        else:
            raise ParseException("No parser matched for: " + cells_of_one_tab)  # no parser matched

    return {"result": output}


@route('/')
def greet():
    return template('template/index.html')


run(host='localhost', port=8080, debug=True)
