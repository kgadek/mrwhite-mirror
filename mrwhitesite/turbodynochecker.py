from contextlib import contextmanager
from functools import wraps
from inspect import signature
from itertools import chain


class TurboDynoMismatch(Exception):
    pass


@contextmanager
def exception_map(e_from, e_to, e_ignored=None):
    try:
        yield
    except e_ignored:
        pass
    except e_from:
        raise e_to


def type_match(value, type_dict, dict_key) -> bool:
    if dict_key not in type_dict:
        return True

    try:
        if isinstance(value, type_dict[dict_key]):
            return True
    except TypeError:
        pass

    try:
        if type_dict[dict_key](value) is True:
            return True
    except TypeError:
        return False


def t_any(_):
    return True


def t_or(*type_params):
    def wrapper(value):
        for type_param in type_params:
            try:
                if type_match(value, {None: type_param}, None):
                    return True
            except TurboDynoMismatch:
                pass
        msg = "Expected a value of type {0}, got ({1} : {2})".format(" | ".join(type_params), value, type(value))
        raise TurboDynoMismatch(msg)  # not a combination of types

    wrapper.__name__ = "t_or({0})".format(" | ".join(str(t) for t in type_params))
    return wrapper


def t_dict(type_param_key, type_param_val):
    def wrapper(dct):
        if not isinstance(dct, dict):
            msg = ("Expected a dict of ({0} : {1}), got ({2} : {3})"
                   .format(type_param_key, type_param_val, dct, type(dct)))
            raise TurboDynoMismatch(msg)  # expected a dict
        for val_k, val_v in dct.items():
            if (not type_match(val_k, {None: type_param_key}, None)
                    or not type_match(val_v, {None: type_param_val}, None)):
                msg = "Expected a dict of ({0} : {1}), got a dict containing ({2} : {4}) : ({3} : {5})".format(
                    type_param_key, type_param_val, val_k, type(val_k), val_v, type(val_v))
                raise TurboDynoMismatch(msg)  # dict key/value type mismatch
        return True

    wrapper.__name__ = "t_dict({0} : {1})".format(type_param_key, type_param_val)
    return wrapper


def t_list(type_param):
    def wrapper(lst):
        if not isinstance(lst, list):
            msg = "Expected a list of {0}, got ({1} : {2})".format(type_param, lst, type(lst))
            raise TurboDynoMismatch(msg)  # expected a dict
        for val in lst:
            if not type_match(val, {None: type_param}, None):
                msg = "Expected a list of {0}, got a list containing: ({1} : {2})".format(type_param, val, type(val))
                raise TurboDynoMismatch(msg)  # list element type mismatch
        return True

    wrapper.__name__ = "t_list({0})".format(type_param)
    return wrapper


def turbodynochecked(typechecked_function):
    @wraps(typechecked_function)
    def wrapper(*args, **kwargs):
        def type_to_str(ty) -> str:
            if ty in anns:
                return anns[ty].__name__
            return "??"

        anns = typechecked_function.__annotations__
        exc_msg_head = ("While typechecking arguments/return values for function\n\t{0} (\n\t\t{1}\n\t) -> {2}"
                        .format(typechecked_function.__name__,
                                ", \n\t\t".join(
                                    "({0} : {1})".format(key, type_to_str(key)) for key, val in
                                    chain(zip(signature(typechecked_function).parameters, args),
                                          kwargs.items())),
                                type_to_str("return")))

        try:
            for i, (key, val) in enumerate(chain(zip(signature(typechecked_function).parameters,
                                                     args),
                                                 kwargs.items())):
                try:
                    if not type_match(val, anns, key):
                        msg = "Type mismatch: expected {0}, got ({1} : {2})".format(type_to_str(key), val,
                                                                                    type(val))
                        raise TurboDynoMismatch(msg)  # argument type mismatch
                except TurboDynoMismatch as e:
                    msg = "While checking argument #{0} : {1}".format(i, type_to_str(key))
                    raise TurboDynoMismatch("\n".join(chain([msg], e.args)))

            typechecked_function_result = typechecked_function(*args, **kwargs)  # turbodynochecker: call given f-n

            if not type_match(typechecked_function_result, anns, "return"):
                msg = "Function {0} was expected to return {1} but got ({2} : {3})".format(
                    typechecked_function.__name__,
                    anns['return'],
                    typechecked_function_result,
                    type(typechecked_function_result))
                raise TurboDynoMismatch(msg)  # function return type mismatch

            return typechecked_function_result

        except TurboDynoMismatch as e:
            e.__context__ = None
            raise TurboDynoMismatch("\n".join(chain([exc_msg_head], e.args)))

    return wrapper
