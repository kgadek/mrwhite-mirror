import re


class ParseException(Exception):
    pass


def safe_match(value, pattern):
    match = re.match(value, pattern)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern)
    return match


def safe_search(value, pattern):
    match = re.search(value, pattern)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern)
    return match