import re
import itertools
from model.model import Model
from parsing.utils import ParseException

from turbodynochecker import turbodynochecked, t_any, t_list, t_dict


# noinspection PyPep8Naming

def FindCellByContent(rows, value):
    for row in rows:
        for cell in row:
            if cell['content'] == value:
                return cell
    return None


def FindCellCoordByContent(rows, value, from_row=0, to_row=-1):
    if to_row == -1:
        to_row = len(rows)
    for row in range(from_row, to_row):
        for cell in range(len(rows[row])):
            if rows[row][cell]['content'] == value:
                return row, cell
    return None


def FindCellCoordByReContent(rows, pattern, row_range=(0, -1)):
    from_row = row_range[0]
    to_row = row_range[1]
    if to_row == -1:
        to_row = len(rows)
    for row in range(from_row, to_row):
        for col in range(len(rows[row])):
            if rows[row][col]['content'] and re.search(pattern, rows[row][col]['content']):
                return row, col
    return None


def ParseLecturesBlock(rows, model, start, colors, prev_spec):
    hour_pattern = r'(\d{1,2}:\d{2})-(\d{1,2}:\d{2})'
    #date_pattern = r'((\d{1,2})\.(\d{1,2}))'
    date_pattern = r'(\d{4})\-(\d{2})\-(\d{2})'
    room_pattern = r'\d{1,2}\.\d{1,2}[a-z]?'
    #lecture_pattern = r'[A-Z\_]+'
    lecture_pattern = r'''
                       ([a-zA-Z_-]+)           # trivia: nazwa przedmiotu
                       (                        # capture group komentarza
                         (                       # łap zero lub jeden
                           \s                     # biały znak
                           \(                     # nawias
                           [a-zA-Z. 0-9]+         # treść komentarza
                           \)                     # nawias
                         )?
                       )
                       '''
    re_lecture = re.compile(lecture_pattern, re.VERBOSE)

    spec_pattern = r'[\w\s]+'

    spec_code = prev_spec
    if start[0] - 1 >= 0:
        upper_cell = rows[start[0] - 1][start[1]]
        upper_content = upper_cell['content']
        if upper_content:
            match = re.search(spec_pattern, upper_content)
            if match:
                spec_code = match.group().strip()

    # wyciaganie wymiarow bloku
    dates_num = 0
    for col in range(1, len(rows[start[0]])):
        if rows[start[0]][col]['content'] and re.search(date_pattern, rows[start[0]][col]['content']):
            dates_num += 1
        else:
            break
    hours_num = 0
    while len(rows) > start[0] + hours_num + 1 and rows[start[0] + hours_num + 1][start[1]]['content'] and re.search(
            hour_pattern, rows[start[0] + hours_num + 1][start[1]]['content']):
        hours_num += 1

    # iteracja po danych w bloku
    for pair in itertools.product(range(0, hours_num, 2), range(dates_num)):
        cell = rows[start[0] + pair[0] + 1][start[1] + pair[1] + 1]
        cell_below = rows[start[0] + pair[0] + 2][start[1] + pair[1] + 1]

        ltype = 'W'
        if cell['bgcolor'] == colors[1]:
            ltype = 'L'

        date_cell = rows[start[0]][start[1] + pair[1] + 1]
        time_cell = rows[start[0] + pair[0] + 1][start[1]]

        datematch = re.match(date_pattern, date_cell['content'])
        hoursmatch = re.match(hour_pattern, time_cell['content'])

        if datematch and hoursmatch:
            day = int(datematch.group(3))
            month = int(datematch.group(2))
            year = int(datematch.group(1))
            hours_from = hoursmatch.group(1)
            hours_to = hoursmatch.group(2)

            room0 = None
            room1 = None

            if cell['content']:
                roommatch = re.search(room_pattern, cell['content'])
                if roommatch:
                    room0 = roommatch.group()

            if cell_below['content']:
                roommatch = re.search(room_pattern, cell_below['content'])
                if roommatch:
                    room1 = roommatch.group()

            if cell['content']:
                lecturematch = re_lecture.search(cell['content'])
                lecture_code = lecturematch and lecturematch.group() or None
                lecture_code_below = None

                if cell_below['content']:
                    lecturematch_below = re_lecture.search(cell_below['content'])
                    lecture_code_below = lecturematch_below and lecturematch_below.group() or None

                if room0 is None and lecture_code:
                    # add to both groups
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room1, 0,
                                     spec_code)
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room1, 1,
                                     spec_code)
                elif lecture_code and lecture_code_below:
                    # add to both groups
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room1, 0,
                                     spec_code)
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code_below, None, room1, 1,
                                     spec_code)
                elif lecture_code:
                    # add separate
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room0, 0,
                                     spec_code)
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room1, 1,
                                     spec_code)
    return spec_code


@turbodynochecked
def ParseSemester(rows: t_list(t_list(t_dict(t_any, t_any))),
                  model: Model):
    lecture_color = FindCellByContent(rows, u'Wykład')['bgcolor']
    lab_color = FindCellByContent(rows, u'Lab.')['bgcolor']

    block_pattern = r'Pn|Wt|Śr|Cz|Pt|Sb|Nd|Legenda'

    start = (0, -1)
    spec = None
    blocks = 0
    while True:
        start = FindCellCoordByReContent(rows, block_pattern, (start[0] + 1, -1))
        if start is None:
            break
        spec = ParseLecturesBlock(rows, model, start, [lecture_color, lab_color], spec)
        blocks += 1

    if blocks == 0:
        raise ParseException(u'no blocks detected')
