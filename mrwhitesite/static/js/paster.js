// http://stackoverflow.com/questions/2176861/javascript-get-clipboard-data-on-paste-event-cross-browser

var welcome_message = "Wklej plan w formacie xls";
var pasted_message = "Sprawdź poprawność wklejonych danych";

var pasted_buffer = [];

$(document).ready(function () {
    $('.after-paste').hide();
});


function handlepaste(elem, e) {
    var savedcontent = elem.innerHTML;
    if (e && e.clipboardData && e.clipboardData.getData) {// Webkit - get data from clipboard, put into editdiv, cleanup, then cancel event
        if (/text\/html/.test(e.clipboardData.types)) {
            elem.innerHTML = e.clipboardData.getData('text/html');
        }
        else if (/text\/plain/.test(e.clipboardData.types)) {
            elem.innerHTML = e.clipboardData.getData('text/plain');
        }
        else {
            elem.innerHTML = "";
        }
        waitforpastedata(elem, savedcontent);
        if (e.preventDefault) {
            e.stopPropagation();
            e.preventDefault();
        }
        return false;
    }
    else {// Everything else - empty editdiv and allow browser to paste content into it, then cleanup
        elem.innerHTML = "";
        waitforpastedata(elem, savedcontent);
        return true;
    }
}

function waitforpastedata(elem, savedcontent) {
    if (elem.childNodes && elem.childNodes.length > 0) {
        processpaste(elem, savedcontent);
    }
    else {
        that = {
            e: elem,
            s: savedcontent
        }
        that.callself = function () {
            waitforpastedata(that.e, that.s)
        }
        setTimeout(that.callself, 20);
    }
}

function select_result_content() {
    var subject = $('#resultmessage');
    subject.focus();
    subject.select();
}

function denyHandler() {

    $('#previewdiv').remove();
    $('#message').text(welcome_message);
    $('.after-paste').hide();

}

function acceptHandler(pasteddata) {

    pasted_buffer.push(pasteddata);
    var sofarelem = $('#pasted-so-far');
    // var sofar = sofarelem.text();

    sofarelem.text(pasted_buffer.length);


    $('#previewdiv').remove();
    $('#message').text(welcome_message);
    $('.after-paste').hide();
}

function processpaste(elem, savedcontent) {
    pasteddata = elem.innerHTML;
    //^^Alternatively loop through dom (elem.childNodes or elem.getElementsByTagName) here

    elem.innerHTML = savedcontent;

    // Do whatever with gathered data;
    // alert(pasteddata);

    // window.testt = pasteddata;
    pasteddata = pasteddata.replace(/\&lt;/g, '<').replace(/\&gt;/g, '>');

    $('#denyButton').off('click').on('click', denyHandler);
    $('#acceptButton').off('click').on('click', function () {
        acceptHandler(pasteddata);
        return false;
    });

    display_pasted_table(pasteddata, elem);

    $('#message').text(pasted_message);

    $('.after-paste').show();
    console.log(pasteddata);
    function submitHandler() {
        $.ajax({
            type: "POST",
            url: "/posthere",
            data: JSON.stringify({pastedtext: pasted_buffer}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                // alert(data);
                console.log(data['result']);
                var result_area = $('#resultmessage');
                $('.after-generate').show();
                result_area.text(data['result']);
                select_result_content();
                result_area.off('click').on('click', select_result_content);

            },
            failure: function (err) {
                alert(err);
            }
        });
    }

    $('#submitButton').off('click').on('click', submitHandler);
}

function display_pasted_table(pasteddata, parent) {

    var testdiv = $('<div id="previewdiv" style="overflow:auto;height:300px;width:100%;"></div>');
    // testdiv.attr('contenteditable','false');
    // testdiv.innerHTML = '';
    parent.innerHTML = "";
    $(parent).after(testdiv);
    testdiv.html(pasteddata);
    // var pastedtable = testdiv.getElementsByTagName('table')[0];
    // testdiv.appendChild(pastedtable);
    // testdiv.appendChild(pasteddata);
    // parent.appendChild(testdiv);

}

// var testdiv = $('<div id=test></div>');
// testdiv.innerHTML = pasteddata;
// var pastedtable = testdiv.getElementsByTagName('table')[0];
// testdiv.innerHTML = '';
// testdiv.appendChild(pastedtable);

