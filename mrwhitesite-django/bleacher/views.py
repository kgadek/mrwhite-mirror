from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

import os
from django.conf import settings

from bleacher.models import Document
from bleacher.forms import DocumentForm

# Create your views here.

def index(req):
    return HttpResponse("Hello, world. You're at the bleacher index.")


def list(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()

            print("UPLOAD:\n\tdir: {}\n\tfile: {}".format(settings.MEDIA_ROOT, request.FILES['docfile']))

            return HttpResponseRedirect(reverse('bleacher.views.list'))
    else:
        form = DocumentForm()

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'bleacher/list.html',
        {'documents': documents, 'form': form},
        context_instance=RequestContext(request)
    )